# Brynne DuBois and Cat Murad
# Professor Wang
# CSE 40437
# Oscar Crawler

# Keywords relating to Best Actress - Includes nominee names and the films associated with them
import re
import sys
import preprocessor as p
import tweepy
from tweepy import OAuthHandler
from textblob import TextBlob
import numpy as np
import math
import matplotlib.pyplot as plt

# Define the keys/tokens and secrets (given to me by Twitter Developer)
CONSUMER_KEY = 'q1DjgCu0bxVHU2DT5MsW1LOLr'
CONSUMER_SECRET = 'iYqmq8qWTgIuy2RsoQq2CcMM4yT6gDqwAZE1mp0GchjZQMDH0F'
ACCESS_TOKEN = '1087393107358957568-GSvCkBFpOC5J30byfNFi2jB2Pmdiib'
ACCESS_SECRET = 'CqzArrlunWlNqdNJvXHul5teMAvuqDfVF0tYH2jEGwDZb'

auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
api = tweepy.API(auth)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

# Search capabilities
api = tweepy.API(auth)
test = api.lookup_users(user_ids=['34373370, 26257166, 12579252'])

# Find tweets for a specified keyword(s)
#f = open('best_actress.txt', 'w')
f = open(sys.argv[1], 'w')
query = '"Olivia Colman" OR "olivia colman" OR "#OliviaColman" OR #oliviacolman"'
maxTweets = 1000 # Return 1000 tweets
tweets = tweepy.Cursor(api.search,q=query).items(maxTweets)
posCount = 0
negCount = 0
neuCount = 0
for tweet in tweets:
    p.set_options(p.OPT.URL, p.OPT.EMOJI, p.OPT.RESERVED, p.OPT.SMILEY)
    clean_tweets = p.clean(tweet.text)
    f.write(str(tweet.created_at) + "\n" + clean_tweets + "\n") # Tweets containing the desired keywords
    analysis = TextBlob(tweet.text)
    f.write(str(analysis.sentiment) + '\n')
    if analysis.sentiment[0]>0:
        f.write('Positive\n')
        posCount += 1
    elif analysis.sentiment[0]<0:
        f.write('Negative\n')
        negCount += 1
    else:
        f.write('Neutral\n')
        neuCount += 1
    f.write('\n')

# Graph sentiments
objects = ['Positive', 'Negative', 'Neutral']
no_sentiments = [posCount, negCount, neuCount]
index = np.arange(len(objects))
plt.bar(index, no_sentiments, width=0.2)
plt.xlabel('Sentiment Type', fontsize=5)
plt.ylabel('Number of Tweets with Sentiment', fontsize=5)
plt.xticks(index, objects, fontsize=5, rotation=30)
plt.title('Oscar Tweet Sentiments for Best Actress')
plt.show() 


f.write("The number of Positive tweets: " + str(posCount) + "\n")
f.write("The number of Negative tweets: " + str(negCount) + "\n")
f.write("The number of Neutral tweets: " + str(neuCount) + "\n")
f.write("The total number of tweets analyzed is: " + str(posCount+negCount+neuCount) + "\n")
f.close() # Close file

