Brynne DuBois and Cat Murad <br />
Professor Wang <br />
CSE 40437 <br />
Semester Project <br />

We built a Python tweet sentiment analyzer using Tweepy, TextBlob, Preprocessor, and GetOldTweets <br />
All of our code/data pertaining to the night of the Oscars can be found within the Best_Actor, Best_Actress, etc folders within this directory. For the rounded out data and predictor data, the code/data can be found within the Data folder of this directory. This was the code that utilized GetOldTweets <br />
To run our code, we would enter the following: <br />
python filename.py output_filename.txt <br />
This would output the tweets pulled as well as the sentiment analysis for each tweet into the specified text file. We ran the code with different keywords depending on which category we were analyzing. 
